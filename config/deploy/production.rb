role :app, %w{luka@grafmuvi.westeurope.cloudapp.azure.com}

server "grafmuvi.westeurope.cloudapp.azure.com", user: "luka", roles: %w{app}
